#include "GBA.h"

#include "TeamKNOx/TeamKNOxLib.h"

// Reversi constants
#include "ZeroMineConstants.h"

extern u16 ViewOpening();
extern u16 ViewLevelSelect();
extern u16 ViewGame();

u16 gViewNumber;
u16 gGameLevel;

u16 AgbMain(void)
{
	gGameLevel = 1;

	SetMode( MODE_3 | BG2_ENABLE ); // Set MODE3

	gViewNumber = KViewOpening;
	ViewOpening();

	gViewNumber = KViewLevelSelect;
	while(1){
		switch (gViewNumber){
			case KViewOpening:
				ViewOpening();
			case KViewLevelSelect:
				ViewLevelSelect();
				break;
			case KViewGame:
				ViewGame();
				break;

			default:
				ViewOpening();
				break;
		}
	}
	return 0;

}

// EOF
