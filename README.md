# MineSweeper for GBA
Typical MineSweeper game for GBA

<table>
<tr>
<td><img src="./pics/ZeroMine1.png"></td>
<td><img src="./pics/ZeroMine2.png"></td>
<td><img src="./pics/ZeroMine3.png"></td>
<td><img src="./pics/ZeroMine4.png"></td>
</tr>
</table>

## BONSAI-Ware
Usually, developed software is excuted on flash cartridge with GBA itself. However, Flash Cartridge is very expensive. GBA can excute not only Flash Cartridge but also Internal RAM with boot functionarity. It is possible to execute with our developed ULA. 

### What's BONSAI ?
Do you know BONSAI ? Nice description is (https://en.wikipedia.org/wiki/Bonsai)[here]. BONSAI realizes a world and universe in a very small pot with limited resource. 

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
