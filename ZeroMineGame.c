#include "GBA.h"

#include "TeamKNOx/extTeamKNOxLib.h"

// Reversi constants
#include "ZeroMineGame.h"

// Application Specfic part
// Graphic(Bitmap) data
// Defined at ReversiEngine part...
#include "bitmaps/gameBoard.c"
#include "bitmaps/HiddenPanel.c"
#include "bitmaps/OpenPanel_0.c"
#include "bitmaps/OpenPanel_1.c"
#include "bitmaps/OpenPanel_2.c"
#include "bitmaps/OpenPanel_3.c"
#include "bitmaps/OpenPanel_4.c"
#include "bitmaps/OpenPanel_5.c"
#include "bitmaps/OpenPanel_6.c"
#include "bitmaps/OpenPanel_7.c"
#include "bitmaps/OpenPanel_8.c"
#include "bitmaps/flag.c"

#include "bitmaps/mine.c"
#include "bitmaps/mine_missed.c"


// Non compressed bitmap data
#include "bitmaps/FingerCursorReady.c"
#include "bitmaps/FingerCursorReady_mask.c"


#include "ZeroMineEngine.h"

extern u16 gViewNumber;
extern u16 gGameLevel;

// Engine
extern void setUpBrd();
extern s16 getMineInfo(s16 aFingerCursorPosX, s16 aFingerCursorPosY);
extern s16 gameCheck();

extern s16 brdBaseInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
extern s16 brdMineInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
extern s16 brdMineChecked[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
extern s16 brdFlagInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
extern u16 m_mineCount;


void setUpInfo()
{
	u16 posX, posY;

	posX = BOARD_START_POSITION_X * 2 + BOARD_GRID_LENGTH;

	posY = SUB_TITLE_PANE_SIZE_Y + BOARD_START_POSITION_Y * 14;
	DrawText(posX, SUB_TITLE_PANE_SIZE_Y + BOARD_START_POSITION_Y * 14, "LEVEL:", RGB(0, 0, 0), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
	num02str(gGameLevel + 1);
	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
//	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, OFF_SCREEN_ADDRESS);

	posY = SUB_TITLE_PANE_SIZE_Y + BOARD_START_POSITION_Y * 18;
	DrawText(posX, posY, "Time:", RGB(0, 0, 0), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
//	DrawText(posX, posY, "Time:", RGB(0, 0, 0), BG_COLOR, OFF_SCREEN_ADDRESS);
	num02str(m_SecCount);
	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
//	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, OFF_SCREEN_ADDRESS);

	posY = SUB_TITLE_PANE_SIZE_Y + BOARD_START_POSITION_Y * 20;
	DrawText(posX, posY, "Mine:", RGB(0, 0, 10), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
//	DrawText(posX, posY, "Mine:", RGB(0, 0, 10), BG_COLOR, OFF_SCREEN_ADDRESS);
	num02str(m_FlagCount);
	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, TRANSPARENT_OFF, OFF_SCREEN_ADDRESS);
//	DrawText(posX + 8 * 6, posY, gWorkStr, RGB(0, 0, 0), BG_COLOR, OFF_SCREEN_ADDRESS);
}


void setUpPanel()
{
	s16 positionX, positionY, i, j;

	for(j = 0;j < BOARD_GRID_NUMBER;j++){
		for(i = 0;i < BOARD_GRID_NUMBER;i++){
			positionX = i * BOARD_GRID_SIZE + BOARD_START_POSITION_X;
			positionY = j * BOARD_GRID_SIZE + BOARD_START_POSITION_Y;

			if(brdMineChecked[i][j]){
				switch(brdMineInfo[i][j]){
					case 0:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_0_Map, OFF_SCREEN_ADDRESS);
						break;
					case 1:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_1_Map, OFF_SCREEN_ADDRESS);
						break;
					case 2:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_2_Map, OFF_SCREEN_ADDRESS);
						break;
					case 3:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_3_Map, OFF_SCREEN_ADDRESS);
						break;
					case 4:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_4_Map, OFF_SCREEN_ADDRESS);
						break;
					case 5:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_5_Map, OFF_SCREEN_ADDRESS);
						break;
					case 6:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_6_Map, OFF_SCREEN_ADDRESS);
						break;
					case 7:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_7_Map, OFF_SCREEN_ADDRESS);
						break;
					case 8:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_8_Map, OFF_SCREEN_ADDRESS);
						break;
					default:
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)HiddenPanel_Map, OFF_SCREEN_ADDRESS);
						break;
				}
			}
			else{
				BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)HiddenPanel_Map, OFF_SCREEN_ADDRESS);
			}

			switch(brdFlagInfo[i][j]){
				case 1:
					BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)flag_Map, OFF_SCREEN_ADDRESS);
				break;

			default:
				break;
			}

		}
	}
}

void setUpResult()
{
	s16 positionX, positionY, i, j;

	for(j = 0;j < BOARD_GRID_NUMBER;j++){
		for(i = 0;i < BOARD_GRID_NUMBER;i++){
			positionX = i * BOARD_GRID_SIZE + BOARD_START_POSITION_X;
			positionY = j * BOARD_GRID_SIZE + BOARD_START_POSITION_Y;


			if(brdBaseInfo[i][j]){
				BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)mine_Map, OFF_SCREEN_ADDRESS);
			}
			else{
				if(brdFlagInfo[i][j]){
					BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)mine_missed_Map, OFF_SCREEN_ADDRESS);
				}
				else{
					if(brdMineChecked[i][j]){
						switch(brdMineInfo[i][j]){
							case 0:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_0_Map, OFF_SCREEN_ADDRESS);
								break;
							case 1:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_1_Map, OFF_SCREEN_ADDRESS);
								break;
							case 2:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_2_Map, OFF_SCREEN_ADDRESS);
								break;
							case 3:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_3_Map, OFF_SCREEN_ADDRESS);
								break;
							case 4:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_4_Map, OFF_SCREEN_ADDRESS);
								break;
							case 5:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_5_Map, OFF_SCREEN_ADDRESS);
								break;
							case 6:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_6_Map, OFF_SCREEN_ADDRESS);
								break;
							case 7:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_7_Map, OFF_SCREEN_ADDRESS);
								break;
							case 8:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)OpenPanel_8_Map, OFF_SCREEN_ADDRESS);
								break;
							default:
								BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)HiddenPanel_Map, OFF_SCREEN_ADDRESS);
								break;
						}
					}
					else{
						BitBltMaskedComp(positionX, positionY, BOARD_GRID_SIZE, BOARD_GRID_SIZE, (u16*)HiddenPanel_Map, OFF_SCREEN_ADDRESS);
					}
				}
			}
		}
	}
}

void SetUpCursor(u16 aPositionX, u16 aPositionY)
{
	u16 positionX, positionY;

	positionX = aPositionX * BOARD_GRID_SIZE + BOARD_START_POSITION_X;
	positionY = aPositionY * BOARD_GRID_SIZE + BOARD_START_POSITION_Y + CURSOR_FIXED_OFFSET;

	BitBltMasked(positionX, positionY, CURSOR_SIZE_XY, CURSOR_SIZE_XY, (u16*)FingerCursorReady_Map, (u16*)FingerCursorReady_mask_Map, OFF_SCREEN_ADDRESS);

}


void SaveSprite(u16 aPositionX, u16 aPositionY, u32 aVRAM)
{
	u16 i, j;
	u16 positionX, positionY;
	u16* ScreenBuffer;

	positionX = aPositionX * BOARD_GRID_SIZE + BOARD_START_POSITION_X;
	positionY = aPositionY * BOARD_GRID_SIZE + BOARD_START_POSITION_Y + CURSOR_FIXED_OFFSET;

	ScreenBuffer = (u16*)aVRAM;

	for(j = 0;j < 16;j++){
		for(i = 0;i < 16;i++){
			m_BGSaveData[i][j] = ScreenBuffer[(positionY + j) * SCREEN_SIZE_X + positionX + i];
		}
	}
}

void RestoreSprite(u16 aPositionX, u16 aPositionY, u32 aVRAM)
{
	u16 i, j;
	u16 positionX, positionY;
	u16* ScreenBuffer;

	ScreenBuffer = (u16*)aVRAM;

	positionX = aPositionX * BOARD_GRID_SIZE + BOARD_START_POSITION_X;
	positionY = aPositionY * BOARD_GRID_SIZE + BOARD_START_POSITION_Y + CURSOR_FIXED_OFFSET;

	for(j = 0;j < 16;j++){
		for(i = 0;i < 16;i++){
			ScreenBuffer[(positionY + j) * SCREEN_SIZE_X + positionX + i] = m_BGSaveData[i][j];
		}
	}
}

u16 ViewGame()
{
	u16 keyWk;					// current key data
	u16 lastKeyWk;				// prevous key data
	u16 stayThisView;

	u16 cursorPosX, cursorPosY;

	stayThisView = 1;
	cursorPosX = cursorPosY = BOARD_GRID_NUMBER / 2;
	keyWk = lastKeyWk = 0;
	m_TickCount = 0;
	m_SecCount = 0;

	setUpBrd();
	m_FlagCount = m_mineCount;

	BitBltMaskedComp(0, 0, 240, 160, (u16*)gameBoard_Map, OFF_SCREEN_ADDRESS);
	setUpPanel();
	setUpInfo();
	SaveSprite(cursorPosX, cursorPosY, OFF_SCREEN_ADDRESS);

	while(stayThisView){
		keyWk = *KEYS;
		if(!(lastKeyWk ^ keyWk)){
		}
		else{
			RestoreSprite(cursorPosX, cursorPosY, OFF_SCREEN_ADDRESS);

			if(!(*KEYS & KEY_UP)){
				if(cursorPosY > 0){
					cursorPosY--;
				}
			}
			if(!(*KEYS & KEY_DOWN)){
				if(cursorPosY < BOARD_GRID_NUMBER - 1){
					cursorPosY++;
				}
			}
			if(!(*KEYS & KEY_LEFT)){
				if(cursorPosX > 0){
					cursorPosX--;
				}
			}
			if(!(*KEYS & KEY_RIGHT)){
				if(cursorPosX < BOARD_GRID_NUMBER - 1){
					cursorPosX++;
				}
			}
			if(!(*KEYS & KEY_A)){
				if(brdBaseInfo[cursorPosX][cursorPosY]){	// Blast !!
					setUpResult();
					DrawBoxHalf(BOARD_START_POSITION_X / 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 3, BOARD_START_POSITION_X + BOARD_GRID_SIZE * BOARD_GRID_NUMBER, BOARD_GRID_SIZE * 2, OFF_SCREEN_ADDRESS);
					DrawText(BOARD_START_POSITION_X + BOARD_GRID_SIZE * 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 4, "Bombed !!", RGB(30, 30, 30), 0x7FFF,  TRANSPARENT_ON, OFF_SCREEN_ADDRESS);
#if OFF_SCREEN
					Off2VRAM(OFF_SCREEN_ADDRESS, VRAM_ADDRESS);
#endif
					AWait();
					gViewNumber = KViewOpening;
					stayThisView = 0;
				}
				else{
					if(brdFlagInfo[cursorPosX][cursorPosY]){	// Flag Off
						brdFlagInfo[cursorPosX][cursorPosY] = 0;
						m_FlagCount++;
					}
					getMineInfo(cursorPosX, cursorPosY);
					setUpPanel();
					if(gameCheck()){
						// Completed
						DrawBoxHalf(BOARD_START_POSITION_X / 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 3, BOARD_START_POSITION_X + BOARD_GRID_SIZE * BOARD_GRID_NUMBER, BOARD_GRID_SIZE * 2, OFF_SCREEN_ADDRESS);
						DrawText(BOARD_START_POSITION_X + BOARD_GRID_SIZE * 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 4, "Completed !!", RGB(30, 30, 30), 0x7FFF, TRANSPARENT_ON, OFF_SCREEN_ADDRESS);
//						DrawText(BOARD_START_POSITION_X + BOARD_GRID_SIZE * 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 4, "Completed !!", RGB(30, 30, 30), 0x7FFF, OFF_SCREEN_ADDRESS);
#if OFF_SCREEN
						Off2VRAM(OFF_SCREEN_ADDRESS, VRAM_ADDRESS);
#endif
						AWait();
						stayThisView = 0;
					}
				}
			}

			if(!(*KEYS & KEY_B)){
				if(m_FlagCount){
					m_FlagCount--;
					brdFlagInfo[cursorPosX][cursorPosY] = 1;
					setUpInfo();
					setUpPanel();
				}
			}

			SaveSprite(cursorPosX, cursorPosY, OFF_SCREEN_ADDRESS);
			SetUpCursor(cursorPosX, cursorPosY);
		}

		WaitForVsync(); // Wait VBL
		if(m_TickCount++ > TICK_COUNT){
			m_TickCount = 0;
			if(m_SecCount++ >= TIME_UP_SECOND){
				DrawBoxHalf(BOARD_START_POSITION_X / 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 3, BOARD_START_POSITION_X + BOARD_GRID_SIZE * BOARD_GRID_NUMBER, BOARD_GRID_SIZE * 2, OFF_SCREEN_ADDRESS);
				DrawText(BOARD_START_POSITION_X + BOARD_GRID_SIZE * 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 4, "Time Up !!", RGB(30, 30, 30), 0x7FFF, TRANSPARENT_ON, OFF_SCREEN_ADDRESS);
//				DrawText(BOARD_START_POSITION_X + BOARD_GRID_SIZE * 2, BOARD_START_POSITION_Y + BOARD_GRID_SIZE * 4, "Time Up !!", RGB(30, 30, 30), 0x7FFF, OFF_SCREEN_ADDRESS);
				#if OFF_SCREEN
					Off2VRAM(OFF_SCREEN_ADDRESS, VRAM_ADDRESS);
				#endif
				AWait();
				stayThisView = 0;
			}
		}
		setUpInfo();

		#if OFF_SCREEN
			Off2VRAM(OFF_SCREEN_ADDRESS, VRAM_ADDRESS);
		#endif

		lastKeyWk = keyWk;

	}
	gViewNumber = KViewOpening;

	return 0;
}

// EOF
