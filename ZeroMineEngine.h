// ZeroMineEngine.h

#ifndef ZEROMINE_ENGINE
#define ZEROMINE_ENGINE

#include "ZeroMineConstants.h"

s16 brdBaseInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
s16 brdMineInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
s16 brdMineChecked[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];
s16 brdFlagInfo[BOARD_GRID_NUMBER][BOARD_GRID_NUMBER];

u16 m_mineCount;
u16 m_playTime;

#endif

