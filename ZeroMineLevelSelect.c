#include "GBA.h"

#include "TeamKNOx/extTeamKNOxLib.h"

// Reversi constants
#include "ZeroMineConstants.h"

// Application Specfic part
// Graphic(Bitmap) data
#include "bitmaps/LevelSelection.c"

extern u16 gViewNumber;
extern u16 gGameLevel;

#define LEVEL_SELECT_CURSOR_POS_X	60
#define LEVEL_SELECT_CURSOR_STEP	25
#define LEVEL_SELECT_CURSOR_OFFSET	50
#define LEVEL_SELECT_CURSOR_WIDTH	120
#define LEVEL_SELECT_CURSOR_HEIGHT	24
#define LEVEL_SELECT_CURSOR_COLOR	0x7C00

void drawCusorLevelSelect(u16 aCursorPosition, u16 aColor)
{

	DrawBoxEmpty(LEVEL_SELECT_CURSOR_POS_X,
		 aCursorPosition * LEVEL_SELECT_CURSOR_STEP + LEVEL_SELECT_CURSOR_OFFSET,
		 LEVEL_SELECT_CURSOR_WIDTH,
		 LEVEL_SELECT_CURSOR_HEIGHT,
		 aColor,
		 VRAM_ADDRESS);
}

u16 ViewLevelSelect()
{
	u16 keyWk;					// current key data
	u16 lastKeyWk;				// prevous key data
	u16 stayThisView;
	u16 cursorPosY;

	gRandomSeed = *(volatile u32*)0x4000006;

	BitBltMaskedComp(0, 0, SCREEN_SIZE_X, SCREEN_SIZE_Y, (u16*)LevelSelection_Map, OFF_SCREEN_ADDRESS);

#ifdef OFF_SCREEN
	Off2VRAM(OFF_SCREEN_ADDRESS, VRAM_ADDRESS);
#endif

	cursorPosY = gGameLevel;

	lastKeyWk = keyWk = 0;
	stayThisView = 1;
	while(stayThisView){
		gRandomSeed++;

		keyWk = *KEYS;

		if(lastKeyWk ^ keyWk){
			if(!(*KEYS & KEY_UP)){
				if(cursorPosY > 0){
					drawCusorLevelSelect(cursorPosY, BG_COLOR);
					cursorPosY--;
					gRandomSeed = gRandomSeed << 1;
				}
			}
			if(!(*KEYS & KEY_DOWN)){
				if(cursorPosY < 2){
					drawCusorLevelSelect(cursorPosY, BG_COLOR);
					cursorPosY++;
					gRandomSeed = gRandomSeed << 1;
				}
			}
			if(!(*KEYS & KEY_LEFT)){
			}
			if(!(*KEYS & KEY_RIGHT)){
				stayThisView = 0;
			}
			if(!(*KEYS & KEY_A)){
				stayThisView = 0;
			}
			if(!(*KEYS & KEY_B)){
			}
			drawCusorLevelSelect(cursorPosY, LEVEL_SELECT_CURSOR_COLOR);
		}

		lastKeyWk = keyWk;
		WaitForVsync(); // Wait VBL


	}
	switch(cursorPosY){
		case 0:
			gGameLevel = 2;
			break;
		case 1:
			gGameLevel = 1;
			break;
		case 2:
			gGameLevel = 0;
		default:
			break;
	}

	gViewNumber = KViewGame;

	return 0;
}

// EOF
