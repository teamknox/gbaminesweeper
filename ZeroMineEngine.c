#include "gba.h"

// ZeroMineAdvance Engine part

#include "ZeroMineEngine.h"
#include "TeamKNOx/extTeamKNOxLib.h"

extern u16 gGameLevel;

// ================================================================
//
//
//
// ================================================================

void clrBrdInfo()
{
	s16 i, j;
	for(j = 0;j < BOARD_GRID_NUMBER;j++){
		for(i = 0;i < BOARD_GRID_NUMBER;i++){
			brdBaseInfo[i][j] = 0;
			brdMineInfo[i][j] = 0;
			brdMineChecked[i][j] = 0;
			brdFlagInfo[i][j] = 0;
		}
	}
}

void clrPlayTime()
{
	m_playTime = 0;
}

void setUpBrd()
{
	s16 i, j, loopControl;

	clrBrdInfo();
	clrPlayTime();

	m_mineCount = (gGameLevel + 2) * 5;
	loopControl = m_mineCount;

	seed_lc(gRandomSeed);

	while(loopControl){
		i = random_lc() % BOARD_GRID_NUMBER;
		j = random_lc() % BOARD_GRID_NUMBER;
		if(brdBaseInfo[i][j]){
		}
		else{
			brdBaseInfo[i][j] = 1;
			loopControl--;
		}
	}
}

s16 getMineInfo(s16 aFingerCursorPosX, s16 aFingerCursorPosY)
{
	s16 dX, dY;
	s16 aroundX, aroundY;
	s16 conditionScopeX, conditionScopeY;
    s16 sumOfAround;

	sumOfAround  = 0;

	if (!(((aFingerCursorPosX >= 0) && (aFingerCursorPosX < BOARD_GRID_NUMBER)) &&
 	      ((aFingerCursorPosY >= 0) && (aFingerCursorPosY < BOARD_GRID_NUMBER))) ) {
		/* Out of range */
		return 0;
	} else if (brdMineChecked[aFingerCursorPosX][aFingerCursorPosY]) {
		// Blast!!!
		return 1;
    }

	for(dY = -1;dY <= 1;dY++){
		for(dX = -1;dX <= 1;dX++){
			if((dX == 0) && (dY == 0)){
				// Point self
			}
			else{
				aroundX = aFingerCursorPosX + dX;
				aroundY = aFingerCursorPosY + dY;
				conditionScopeX = ((aroundX >= 0) && (aroundX < BOARD_GRID_NUMBER));
				conditionScopeY = ((aroundY >= 0) && (aroundY < BOARD_GRID_NUMBER));

				if(conditionScopeX && conditionScopeY){
					if(brdBaseInfo[aroundX][aroundY]){
						sumOfAround++;
					}
				}
			}
		}
	}


	brdMineInfo[aFingerCursorPosX][aFingerCursorPosY] = sumOfAround;
    brdMineChecked[aFingerCursorPosX][aFingerCursorPosY] = 1;

	if(sumOfAround){
		return 0;
	}
	else{
		for(dY = -1;dY <= 1;dY++){
			for(dX = -1;dX <= 1;dX++){
				if((dX == 0) && (dY == 0)){
					// Point self
				}
				else{
					getMineInfo(aFingerCursorPosX + dX, aFingerCursorPosY + dY);
				}
			}
		}
	}

	return 0;
}

s16 gameCheck()
{
	s16 i, j;
	s16 leftPanel;

	leftPanel = BOARD_GRID_NUMBER * BOARD_GRID_NUMBER - m_mineCount;

	for(j = 0;j < BOARD_GRID_NUMBER;j++){
		for(i = 0;i < BOARD_GRID_NUMBER;i++){
			if(brdMineChecked[i][j]){
				leftPanel--;
			}
		}
	}

	if(leftPanel)
		return 0;
	else
		return 1;

}

